# Copyright (C) 2008-2016 Marvell International Ltd.
# All Rights Reserved.
#

exec-y += aws_starter_demo
aws_starter_demo-objs-y := src/main.c
aws_starter_demo-cflags-y := -I$(d)/src -DAPPCONFIG_DEBUG_ENABLE=1

ifeq ($(board_name-y), vd_iot_evalkit-v1)
global-cflags-y += -DBOARD_NAME=\"$(board_name-y)\"
global-cflags-y += -DBOARD_VD_IOT_EVALKIT=1
endif

ifeq ($(SEN_OCC),y)
global-cflags-y += -DSEN_OCC=1
endif

ifeq ($(SEN_LIGHT),y)
global-cflags-y += -DSEN_LIGHT=1
endif

ifeq ($(SEN_PRESSURE),y)
global-cflags-y += -DSEN_PRESSURE=1
endif

ifeq ($(SEN_TH),y)
global-cflags-y += -DSEN_TH=1
endif

ifeq ($(SEN_GAS),y)
global-cflags-y += -DSEN_GAS=1
endif

ifeq ($(SEN_CO2),y)
global-cflags-y += -DSEN_CO2=1
endif

ifeq ($(SEN_TEMPR),y)
global-cflags-y += -DSEN_TEMPR=1
endif

ifeq ($(SEN_ACC),y)
global-cflags-y += -DSEN_ACC=1
endif

ifeq ($(SEN_ULTRASONIC),y)
global-cflags-y += -DSEN_ULTRASONIC=1
endif

#ifneq ($(wildcard $(d)/www),)
aws_starter_demo-ftfs-y := aws_starter_demo.ftfs
aws_starter_demo-ftfs-dir-y     := $(d)/www
aws_starter_demo-ftfs-api-y := 100
#endif

# Applications could also define custom linker files if required using following:
#aws_starter_demo-linkerscript-y := /path/to/linkerscript
# Applications could also define custom board files if required using following:
#aws_starter_demo-board-y := /path/to/boardfile
