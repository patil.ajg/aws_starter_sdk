# Copyright (C) 2008-2016, Marvell International Ltd.
# All Rights Reserved.

global-cflags-y += -I$(d)/incl -I$(d)/incl/grove -I$(d)/incl/generic

-include $(d)/build.sensors.mk
